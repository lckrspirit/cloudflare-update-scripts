#!/bin/bash

# Change static vars:
CF_ZONE="example.com"
CF_ARECORD="service.example.com"
CF_TOKEN=""
LOG_PATH="/var/log/cfupdate.log"

IP_ADDR=$(curl -s -X GET https://checkip.amazonaws.com)

NGNX_AVABILITY=$(curl -s $CF_ARECORD >>/dev/null && echo up || echo down)
HOST_AVABILITY=$(ping -c3 $CF_ARECORD >>/dev/null && echo up || echo down)

CF_ZONEID=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$CF_ZONE&status=active" \
  -H "Authorization: Bearer $CF_TOKEN" \
  -H "Content-Type: application/json" | jq -r '{"result"}[] | .[0] | .id')

CF_ARECORDID=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$CF_ZONEID/dns_records?type=A&name=$CF_ARECORD" \
  -H "Authorization: Bearer $CF_TOKEN" \
  -H "Content-Type: application/json" | jq -r '{"result"}[] | .[0] | .id')

CF_CURRENT_IP=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$CF_ZONEID/dns_records/$CF_ARECORDID" \
  -H "Authorization: Bearer $CF_TOKEN" \
  -H "Content-Type: application/json" | jq -r '{"result"}[] | .content')

LOG_PATH="/var/log/cfupdate.log"

function message_to_file() {
    MESSAGE=$1
    TIMESTAMP=$(date "+%Y-%m-%d/%H:%M")
    echo "$TIMESTAMP --> $MESSAGE" >> $LOG_PATH
}

function update_dns_record() {
    curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONEID/dns_records/$CF_ARECORDID" \
        -H "Authorization: Bearer $CF_TOKEN" \
        -H "Content-Type: application/json" \
        --data "{\"type\":\"A\",\"name\":\"$CF_ARECORD\",\"content\":\"$IP_ADDR\",\"ttl\":900,\"proxied\":false}" | jq
}

if [[ $IP_ADDR == $CF_CURRENT_IP ]]; then
    message_to_file "DNS Record don't needed update"
    exit
fi

if [[ $NGNX_AVABILITY = "down" || $HOST_AVABILITY = "down" ]]; then
    message_to_file "Nginx state is $NGNX_AVABILITY. Host is $HOST_AVABILITY. Perform cf failover."
    update_dns_record
fi